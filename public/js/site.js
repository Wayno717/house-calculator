﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

var mortgageMultiplier = 0.0;
var ltiMortgage = 0;
var ltvMortgage = 0;
var fhs = 0;
var finalV = 0;
var deposit = 0;
var hpValue = 0;
var dpValue = 0;
var minDeposit = 0;
var maxDeposit = 0;
var avalibleMortgage = 0;

var dict = {
    "antrim": 425000,
    "armagh": 425000,
    "carlow": 325000,
    "cavan": 325000,
    "clare": 350000,
    "cork": 425000,
    "derry": 425000,
    "donegal": 325000,
    "down": 425000,
    "dublin": 425000,
    "fermanagh": 425000,
    "galway": 450000,
    "kerry": 325000,
    "kildare": 425000,
    "kilkenny": 375000,
    "laois": 375000,
    "leitrim": 325000,
    "limerick": 425000,
    "longford": 325000,
    "louth": 375000,
    "mayo": 325000,
    "meath": 425000,
    "monaghan": 325000,
    "offaly": 325000,
    "roscommon": 325000,
    "sligo": 325000,
    "tipperary": 325000,
    "tyrone": 425000,
    "waterford": 425000,
    "westmeath": 375000,
    "wexford": 325000,
    "wicklow": 475000
};


var lhalMaxDic = {
    "antrim": 425000,
    "armagh": 425000,
    "carlow": 275000,
    "cavan": 275000,
    "clare": 300000,
    "cork": 330000,
    "derry": 425000,
    "donegal": 275000,
    "down": 360000,
    "dublin": 360000,
    "fermanagh": 425000,
    "galway": 330000,
    "kerry": 275000,
    "kildare": 360000,
    "kilkenny": 300000,
    "laois": 275000,
    "leitrim": 275000,
    "limerick": 300000,
    "longford": 275000,
    "louth": 330000,
    "mayo": 275000,
    "meath": 330000,
    "monaghan": 275000,
    "offaly": 275000,
    "roscommon": 275000,
    "sligo": 275000,
    "tipperary": 275000,
    "tyrone": 425000,
    "waterford": 300000,
    "westmeath": 300000,
    "wexford": 300000,
    "wicklow": 360000
};

function recalculateData() {

    var inEllibable = false;
    if (pcSelect.value == "pcSHH" || !amFtb.checked)
        inEllibable = true;


    useFHS.disabled = inEllibable;
    useHTB.disabled = inEllibable;
    useLHAL.disabled = inEllibable;
    if (inEllibable) {
        useFHS.checked = false;
        useHTB.checked = false;
        useLHAL.checked = false;
    }


    var maxFHSAllowence = dict[cSelect.value];

    hpValue = Number(hpInput.value);
    if (hpValue >= hpInput.max) {
        hpInput.value = hpInput.max;
    }
    hpValue = Number(hpInput.value);

    if (useFHS.checked) {
        if (hpValue > maxFHSAllowence) {
            hpInput.value = maxFHSAllowence;
        }
    } else if (useHTB.checked && useFHS.checked) {
        if (hpValue > maxFHSAllowence) {
            hpInput.value = maxFHSAllowence;
        }
    }
    areaValue.innerHTML = cSelect.value + " area value " + maxFHSAllowence.toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' });

    var fhsHtb = 0.3;

    var beds = bedsOption.value;
    deposit = 0.1;
    if (beds <= 2) {
        deposit = 0.2;
    }

    minDeposit = GetPropertyDeposit();
    ltvMortgage = hpValue * (1.0 - deposit);



    if (useHTB.checked) {
        fhsHtb = 0.2;
        var x = minDeposit - yearTotalValue;
        if (x < 0) {
            x = 0;
        }

    } else {
        fhsHtb = 0.3;
        yearTotalValue = 0;
    }



    dpValue = Number(dpInput.value);
    mLabel.innerHTML = mSlider.value;


    var combinedSalary = Number(tySalaryOne.value);
    combinedSalary += Number(tySalaryTwo.value);

    mortgageMultiplier = 4.0;
    var maxLHALMortgage = lhalMaxDic[cSelect.value];
    if (useLHAL.checked) {
        mortgageMultiplier = 4.25;
    } else if (!amFtb.checked) {
        mortgageMultiplier = 3.5;
    }




    if (oMortgageInput.value > 0) {
        avalibleMortgage = Number(oMortgageInput.value);
    }
    else {
        ltiMortgage = CalculateMaxPossibleMortgage(combinedSalary);
        avalibleMortgage = GetCorrectMortgage(ltiMortgage, ltvMortgage);
        if (useLHAL.checked) {
            avalibleMortgage = GetCorrectMortgage(avalibleMortgage, maxLHALMortgage);
        }
    }

    if (hpValue > 500000) {
        useHTB.checked = false;
        useHTB.disabled = true;
    } else {
        useHTB.disabled = false;
    }


    var outputValue = minDeposit + avalibleMortgage;

    var maxFHS = maxFHSAllowence * fhsHtb;
    var minFHS = hpValue - outputValue;
    fhs = Math.min(maxFHS, minFHS);
    finalV = outputValue + fhs;
    if (finalV <= hpValue)
        finalV = hpValue;


    if (fhs <= 0) {
        useFHS.checked = false;
        useFHS.disabled = true;
        fhs = 0;
    } else {
        useFHS.disabled = false;
    }

    
    if (!useFHS.checked)
        fhs = 0;

    if (useFHS.checked) {
        fhsContainer.style.display = "block";
    } else {
        fhsContainer.style.display = "none";
    }

    var yearTotalValue = GetHelpToBuyValue();





    var resultValue = avalibleMortgage + fhs + dpValue + yearTotalValue
    if (resultValue >= hpValue) {
        rTextP.innerHTML = "Yes, it is technically possible for you to buy a house based on the values you put in. However, it's subject to the bank's lending criteria and whether you are accepted for the schemes/grants/loans above. I wouldn't let that discourage you; however, so come up with a plan for the next year or several."
        rEmoteP.innerHTML = "&#128512;"

    } else {
        rTextP.innerHTML = "No, or rather, based on the information you provided, you either don't have enough to buy a house from a mortgage alone, with the available schemes, or based on the salary you have. You could technically accumulate enough in cash by saving up for years, but your best bet is likely to wait for an economic crash :("
        rEmoteP.innerHTML = "&#128532;"
    }


    if (useHTB.checked) {
        taxIncomeContainer.style.display = "block"
    } else {
        taxIncomeContainer.style.display = "none"
    }

    RedrawLabelValue();

    if (isDer.checked)
        cBonus.style.display = "block";
    else
        cBonus.style.display = "none";

}

function GetHelpToBuyValue() {
    if (!useHTB.checked)
        return 0;

    var year1Value = Number(year1Number.value);
    var year2Value = Number(year2Number.value);
    var year3Value = Number(year3Number.value);
    var year4Value = Number(year4Number.value);
    var yearTotalValue = year1Value + year2Value + year3Value + year4Value;
    if (yearTotalValue > 30000)
        yearTotalValue = 30000;
    return yearTotalValue;
}

function GetPropertyDeposit() {
    return hpValue * deposit;
}


function CalculateMaxPossibleMortgage(salary) {
    var maxMortgage = salary * mortgageMultiplier;
    return maxMortgage;
}

function GetCorrectMortgage(salaryMortgage, houseMaxMortgage) {
    return Math.min(salaryMortgage, houseMaxMortgage)
}

function RedrawLabelValue() {
    maxMortgageLabel.value = ltiMortgage.toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' });

    fhsValue.value = finalV.toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' });
    fhsOutput.value = "+" + fhs.toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' });

    var yearTotalValue = GetHelpToBuyValue();
    yearTotalLabel.innerHTML = yearTotalValue.toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' });
    bSavingLabel.innerHTML = Math.round(dpInput.value / mSlider.value).toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' });
    bDepositLabel.innerHTML = Number(dpInput.value).toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' });
    bHouseLabel.innerHTML = hpValue.toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' });
    totalDownpaymentLabel.innerHTML = Number(dpValue + yearTotalValue).toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' })



    depositNeededLabel.value = Number(minDeposit).toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' });
    mortgageNeededLabel.value = Number(ltvMortgage).toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' });

    bHPLabel.innerHTML = hpValue.toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' })
    bBALabel.innerHTML = avalibleMortgage.toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' })
    bDPLabel.innerHTML = dpValue.toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' })
    bFHSLabel.innerHTML = fhs.toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' })
    bHTBLabel.innerHTML = yearTotalValue.toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' })
    bSDuty.innerHTML = (hpValue * 0.01).toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' })


    bSFee.innerHTML = Number(2000).toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' })
    bSnFee.innerHTML = Number(5000).toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' })
    bVaFee.innerHTML = Number(500).toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' })
    bMovFee.innerHTML = Number(500).toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' })

}

function CalculateMortgage(value) {
    return value * 0.9;
}

var secondPersonGrossIncom = 0;

function UpdateSlider(slider) {
    ssMinLabel.innerHTML = Number(slider.min).toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' });
    ssMaxLabel.innerHTML = Number(slider.max).toLocaleString('ie-EN', { style: 'currency', currency: 'EUR' });
    ssPriceLabel.value = slider.value;
    secondPersonGrossIncom = slider.value;
}

function UpdateInput(input) {
    input.value
}




recalculateData();